BIN := ../lissy
LIB := ../out/liblissy.a

CC := clang
CFLAGS := -Wall -Wextra -Wpedantic 
CC_WIN := x86_64-w64-mingw32-clang

LD := ld.lld
LDFLAGS := -fuse-ld=lld
LIBS := -lreadline -lm
