#include <setjmp.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>

#include <cmocka.h>

#include "eval.h"
#include "lenv.h"
#include "lval.h"
#include "mpc/mpc.h"
#include "parser.h"

#define TEST_CASE(name, prog, result)                                          \
  static void name(void** state) {                                             \
    LEnv* e = lenv_new();                                                      \
    lenv_add_builtins(e);                                                      \
    mpc_result_t ast;                                                          \
    int c = parse_str((prog), &ast);                                           \
    assert_true(c);                                                            \
    LVal* res = eval(e, lval_read(ast.output));                                \
    assert_int_equal(res->num, (result));                                      \
    lval_del(res);                                                             \
    mpc_ast_delete(ast.output);                                                \
    lenv_del(e);                                                               \
  }

#define TEST_CASE_EXEC_EQ(name, prog, prog_ref)                                \
  static void name(void** state) {                                             \
    LEnv* e = lenv_new();                                                      \
    lenv_add_builtins(e);                                                      \
    mpc_result_t ast;                                                          \
    int c = parse_str((prog), &ast);                                           \
    assert_true(c);                                                            \
    LVal* res = eval(e, lval_read(ast.output));                                \
    c = parse_str((prog_ref), &ast);                                           \
    assert_true(c);                                                            \
    LVal* ref = eval(e, lval_read(ast.output));                                \
    int eq = equal_lval(res, ref);                                             \
    if (!eq) {                                                                 \
      printf("Result:    ");                                                   \
      lval_println(res);                                                       \
      printf("Reference: ");                                                   \
      lval_println(ref);                                                       \
    }                                                                          \
    assert_true(eq);                                                           \
    lval_del(res);                                                             \
    lval_del(ref);                                                             \
    mpc_ast_delete(ast.output);                                                \
    lenv_del(e);                                                               \
  }

#define TEST_CASE_LET(name, setup, prog, result)                               \
  static void name(void** state) {                                             \
    LEnv* e = lenv_new();                                                      \
    lenv_add_builtins(e);                                                      \
    mpc_result_t ast;                                                          \
    int c = parse_str((setup), &ast);                                          \
    assert_true(c);                                                            \
    eval(e, lval_read(ast.output));                                            \
    c = parse_str((prog), &ast);                                               \
    assert_true(c);                                                            \
    LVal* res = eval(e, lval_read(ast.output));                                \
    assert_int_equal(res->num, (result));                                      \
    lval_del(res);                                                             \
    mpc_ast_delete(ast.output);                                                \
    lenv_del(e);                                                               \
  }

#define TEST_CASE_ERR(name, prog, error)                                       \
  static void name(void** state) {                                             \
    LEnv* e = lenv_new();                                                      \
    lenv_add_builtins(e);                                                      \
    mpc_result_t ast;                                                          \
    int c = parse_str((prog), &ast);                                           \
    assert_true(c);                                                            \
    LVal* res = eval(e, lval_read(ast.output));                                \
    assert_int_equal(res->type, LVAL_ERR);                                     \
    assert_int_equal(res->err.type, (error));                                  \
    lval_del(res);                                                             \
    mpc_ast_delete(ast.output);                                                \
    lenv_del(e);                                                               \
  }

int equal_lval(LVal* x, LVal* y) {
  if (x->type != y->type)
    return 1;

  switch (x->type) {
  case LVAL_NUM:
    return (x->num == y->num);
    break;
  case LVAL_SYM:
    return (strcmp(x->sym, y->sym) != 0);
    break;
  case LVAL_SEXPR:
  case LVAL_QEXPR:
    if (x->count != y->count)
      return 1;
    int c = 0;
    for (int i = 0; i < x->count; i++) {
      c += equal_lval(x->cell[i], y->cell[i]);
      if (c)
        return 1;
    }
    return c;
    break;
  case LVAL_ERR:
    return (x->err.type == y->err.type) | strcmp(x->err.msg, y->err.msg);
    break;
  default:
    assert_string_equal("eqal_lval", "unknown LVAL_TYPE!");
    break;
  }
  return 1;
}

int setup(void** state) {
  init_parser();
  return 0;
}
int teardown(void** state) {
  del_parser();
  return 0;
}

TEST_CASE(test_inc, "(inc 1)", 2)
TEST_CASE(test_dec, "(dec 2)", 1)
TEST_CASE(test_inc_dec, "(inc (dec 1))", 1)
TEST_CASE(test_neg, "(neg -1)", 1)

TEST_CASE(test_add, "(+ 1 4)", 5)
TEST_CASE(test_add_func, "(add 1 4)", 5)
TEST_CASE(test_sub, "(- 9 4)", 5)
TEST_CASE(test_sub_func, "(sub 9 4)", 5)
TEST_CASE(test_mul, "(* 5 5)", 25)
TEST_CASE(test_mul_func, "(mul 5 5)", 25)
TEST_CASE(test_div, "(/ 16 4)", 4)
TEST_CASE(test_div_func, "(div 16 4)", 4)
TEST_CASE_ERR(test_div_by_zero, "(div 1 0)", ERR_DIV_BY_ZERO)
TEST_CASE(test_mod, "(% 16 3)", 1)
TEST_CASE(test_mod_func, "(mod 16 3)", 1)
TEST_CASE(test_pow, "(^ 2 4)", 16)
TEST_CASE(test_pow_func, "(pow 2 4)", 16)
TEST_CASE(test_min, "(min 90 -3 16 -77 4 111)", -77)
TEST_CASE(test_max, "(max -2 68 -111 1232 44)", 1232)

TEST_CASE(test_head, "(head {90 -3 16 -77 4 111})", 90)
TEST_CASE_EXEC_EQ(test_tail, "(tail {-2 68 -111 1232 44})", "{68 -111 1232 44}")
TEST_CASE_EXEC_EQ(test_list, "(list 90 -3 16 -77 4 111)",
                  "{90 -3 16 -77 4 111}")
TEST_CASE_EXEC_EQ(test_join, "(join {1 2 3} {4 5} {6})", "{1 2 3 4 5 6}")
TEST_CASE_EXEC_EQ(test_eval_1, "(eval (tail {tail tail {5 6 7}}))", "{6 7}")
TEST_CASE(test_eval_2, "(head {(+ 1 2) (+ 10 20)})", 3)
TEST_CASE(test_eval_3, "(eval {head (list 1 2 3 4)})", 1)
TEST_CASE_EXEC_EQ(test_eval_4, "(eval {list 1 2 3 4})", "{1 2 3 4}")
TEST_CASE(test_eval_5, "(eval {length (list 1 2 3 4)})", 4)
TEST_CASE(test_length, "(length {1 2 3 4 5})", 5)

TEST_CASE_LET(test_let, "(let {x 2})", "(+ x x)", 4)

int main() {
  const struct CMUnitTest tests[] = {
      cmocka_unit_test(test_inc),         cmocka_unit_test(test_dec),
      cmocka_unit_test(test_inc_dec),     cmocka_unit_test(test_neg),
      cmocka_unit_test(test_add),         cmocka_unit_test(test_add_func),
      cmocka_unit_test(test_sub),         cmocka_unit_test(test_sub_func),
      cmocka_unit_test(test_mul),         cmocka_unit_test(test_mul_func),
      cmocka_unit_test(test_div),         cmocka_unit_test(test_div_func),
      cmocka_unit_test(test_div_by_zero), cmocka_unit_test(test_mod),
      cmocka_unit_test(test_mod_func),    cmocka_unit_test(test_pow),
      cmocka_unit_test(test_pow_func),    cmocka_unit_test(test_min),
      cmocka_unit_test(test_max),         cmocka_unit_test(test_head),
      cmocka_unit_test(test_tail),        cmocka_unit_test(test_list),
      cmocka_unit_test(test_join),        cmocka_unit_test(test_eval_1),
      cmocka_unit_test(test_eval_2),      cmocka_unit_test(test_eval_3),
      cmocka_unit_test(test_eval_4),      cmocka_unit_test(test_eval_5),
      cmocka_unit_test(test_length),      cmocka_unit_test(test_let),
  };
  int failed = cmocka_run_group_tests(tests, setup, teardown);
  return failed;
}
