#!/bin/bash

URL=registry.gitlab.com/lagerfeuer/lissy

docker build -t ${URL} . \
  && docker push ${URL}

