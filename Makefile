all: src

src:
	$(MAKE) -C src/

debug: 
	$(MAKE) -C src/ debug

asan:
	$(MAKE) -C src/ asan

valgrind: debug
	valgrind --leak-check=full --track-origins=yes ./lissy

win:
	$(MAKE) -C src/ -f Makefile.win

win-debug:
	$(MAKE) -C src/ -f Makefile.win debug

test: src
	$(MAKE) -C tests/
	./tests/test

clean:
	$(MAKE) -C src/ clean
	$(MAKE) -C src/ -f Makefile.win clean
	$(MAKE) -C tests/ clean
	rm -rf vgcore.*

.PHONY: src debug asan win win-debug test check valgrind clean
