#ifndef EVAL_H
#define EVAL_H
#include "lenv.h"
#include "lval.h"
#include "mpc/mpc.h"

LVal* eval(LEnv* lenv, LVal* lval);
LVal* eval_sexpr(LEnv* lenv, LVal* lval);

LVal* builtin(LEnv* lenv, LVal* lval, char* op);
LVal* builtin_op(LEnv* lenv, LVal* lval, char* op);
LVal* builtin_qexpr(LEnv* lenv, LVal* lval, char* op);

LVal* eval_op(LEnv* lenv, LVal* val, char* op, LVal* nval);
LVal* eval_unary_op(LEnv* lenv, LVal* val, char* op);

#endif
