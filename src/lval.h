#ifndef LVAL_H
#define LVAL_H

#include "lbuiltin.h"
#include "lenv.h"
#include "mpc/mpc.h"
#include "util.h"

#define INIT_LVAL malloc(sizeof(LVal))

typedef enum {
  LVAL_ERR = 1,
  LVAL_NUM,
  LVAL_SYM,
  LVAL_FUN,
  LVAL_SEXPR,
  LVAL_QEXPR
} LVAL_TYPE;

typedef enum {
  ERR_DIV_BY_ZERO = 0,
  ERR_BAD_OP,
  ERR_BAD_ARG,
  ERR_BAD_SYM,
  ERR_UNKNOWN_FUNCTION,
  ERR_RANGE
} ERROR_TYPE;

struct lval {
  LVAL_TYPE type;
  union {
    // ERR
    struct {
      ERROR_TYPE type;
      char* msg;
    } err;
    // NUM
    long num;
    // SYM
    char* sym;
    // FUN
    lbuiltin fun;
    // SEXPR, QEXPR
    struct {
      int count;
      struct lval** cell;
    };
  };
};

char* lval_type(LVAL_TYPE type);

LVal* lval_num(long num);
LVal* lval_err(ERROR_TYPE err_type, char* fmt, ...);
LVal* lval_sym(char* s);
LVal* lval_fun(lbuiltin fun);
LVal* lval_sexpr();
LVal* lval_qexpr();
void lval_del(LVal* lval);

LVal* lval_read_num(mpc_ast_t* node);
LVal* lval_read(mpc_ast_t* node);
LVal* lval_add(LVal* lval, LVal* val);

LVal* lval_pop(LVal* lval, int i);
LVal* lval_take(LVal* lval, int i);

LVal* lval_join(LVal* a, LVal* b);
LVal* lval_copy(LVal* orig);

void lval_print(LVal* lval);
void lval_println(LVal* lval);
void lval_expr_print(LVal* v, char open, char close);

#endif
