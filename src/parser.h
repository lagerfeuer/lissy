#ifndef PARSER_H
#define PARSER_H
#include "mpc/mpc.h"

static mpc_parser_t* number;
static mpc_parser_t* symbol;
static mpc_parser_t* sexpr;
static mpc_parser_t* qexpr;
static mpc_parser_t* expr;
static mpc_parser_t* lissy;

int init_parser();
void handle_error(const int result, const mpc_result_t res);
int parse_str(char* input, mpc_result_t* res);
int parse_file(char* filename, mpc_result_t* res);
void del_parser();

#endif
