#include "eval.h"
#include "util.h"

#ifdef DEBUG
#include <stdio.h>
#endif

#include <math.h>

LVal* eval(LEnv* lenv, LVal* lval) {
#ifdef DEBUG
  printf("eval %s\n", TYPE(lval));
#endif
  switch (lval->type) {
  case LVAL_SYM: {
    LVal* var = lenv_get(lenv, lval);
    lval_del(lval);
    return var;
  } break;
  case LVAL_SEXPR:
    return eval_sexpr(lenv, lval);
    break;
  default:
    return lval;
    break;
  }
}

LVal* eval_sexpr(LEnv* lenv, LVal* lval) {
  for (int i = 0; i < lval->count; i++)
    lval->cell[i] = eval(lenv, lval->cell[i]);

  for (int i = 0; i < lval->count; i++)
    if (lval->cell[i]->type == LVAL_ERR) {
      LVal* err = lval_pop(lval, i);
      lval_del(lval);
      return err;
    }

  if (lval->count == 0)
    return lval;
  if (lval->count == 1)
    return lval_take(lval, 0);

  LVal* first = lval_pop(lval, 0);
  if (first->type != LVAL_FUN) {
    lval_del(first);
    lval_del(lval);
    return lval_err(ERR_UNKNOWN_FUNCTION,
                    "First element (%s) is not a function!",
                    lval_type(first->type));
  }

  LVal* result = first->fun(lenv, lval);
  lval_del(first);
  return result;
}

LVal* builtin(LEnv* lenv, LVal* lval, char* op) {
  if (STR_EQ(op, "head") || STR_EQ(op, "tail") || STR_EQ(op, "join") ||
      STR_EQ(op, "eval") || STR_EQ(op, "list") || STR_EQ(op, "length"))
    return builtin_qexpr(lenv, lval, op);
  else
    return builtin_op(lenv, lval, op);
}

LVal* builtin_qexpr(LEnv* lenv, LVal* lval, char* op) {
  LTHROWIF_FMT(lval,
               lval->count > 1 && STR_NEQ(op, "join") && STR_NEQ(op, "list"),
               ERR_BAD_ARG, "'%s' expects exactly %i argument (got %i)!", op, 1,
               lval->count);
  for (int i = 0; i < lval->count; i++)
    LTHROWIF(lval, lval->cell[i]->type != LVAL_QEXPR && STR_NEQ(op, "list"),
             ERR_BAD_ARG, "Arguments need to be QExpr!");
  // TODO:
  // not always the case, head {} shall return nil, tail {} shall return {}
  LTHROWIF_FMT(lval, lval->cell[0]->count == 0, ERR_BAD_ARG,
               "Tried to pass '{}' to builtin '%s'!", op);

  LVal* val;
  if (STR_EQ(op, "head")) {
    val = lval_pop(lval_pop(lval, 0), 0);
    lval_del(lval);
    // eval if lval is sexpr
    if (val->type == LVAL_SEXPR)
      return eval(lenv, val);
  } else if (STR_EQ(op, "tail")) {
    val = lval_take(lval_pop(lval, 0), 0);
    lval_del(lval);
  } else if (STR_EQ(op, "join")) {
    for (int i = 0; i < lval->count; i++) {
      LTHROWIF(lval, lval->cell[i]->type != LVAL_QEXPR, ERR_BAD_ARG,
               "'join' expects QExpr as arguments!");
    }
    val = lval_pop(lval, 0);
    while (lval->count)
      val = lval_join(val, lval_pop(lval, 0));

    lval_del(lval);
  } else if (STR_EQ(op, "eval")) {
    LVal* h = lval_pop(lval, 0);
    lval_del(lval);
    h->type = LVAL_SEXPR;
    val = eval(lenv, h);
  } else if (STR_EQ(op, "list")) {
    lval->type = LVAL_QEXPR;
    val = lval;
  } else if (STR_EQ(op, "length")) {
    LVal* qexpr = lval->cell[0];
    val = lval_num(qexpr->count);
    lval_del(lval);
  } else {
    lval_del(lval);
    val = lval_err(ERR_UNKNOWN_FUNCTION, "function: '%s'", op);
  }

  return val;
}

LVal* builtin_op(LEnv* lenv, LVal* lval, char* op) {
  for (int i = 0; i < lval->count; i++)
    LTHROWIF(lval, lval->cell[i]->type != LVAL_NUM, ERR_BAD_ARG,
             "Argument is not a number!");

  LVal* a = lval_pop(lval, 0);
  if (lval->count == 0)
    a = eval_unary_op(lenv, a, op);

  while (lval->count > 0) {
    LVal* b = lval_pop(lval, 0);
    a = eval_op(lenv, a, op, b);
    lval_del(b);
  }
  return a;
}

LVal* eval_op(LEnv* lenv, LVal* val, char* op, LVal* nval) {
#ifdef DEBUG
  printf("handling binary '%s'\n", op);
#endif
  if (STR_EQ(op, "+") || STR_EQ(op, "add"))
    val->num += nval->num;
  else if (STR_EQ(op, "-") || STR_EQ(op, "sub"))
    val->num -= nval->num;
  else if (STR_EQ(op, "*") || STR_EQ(op, "mul"))
    val->num *= nval->num;
  else if (STR_EQ(op, "/") || STR_EQ(op, "div")) {
    LTHROWIF(val, nval->num == 0, ERR_DIV_BY_ZERO, NULL);
    val->num /= nval->num;
  } else if (STR_EQ(op, "%") || STR_EQ(op, "mod"))
    val->num %= nval->num;
  else if (STR_EQ(op, "^") || STR_EQ(op, "pow"))
    val->num = (pow(val->num, nval->num));
  else if (STR_EQ(op, "min")) {
    if (val->num > nval->num)
      val->num = nval->num;
  } else if (STR_EQ(op, "max")) {
    if (val->num < nval->num)
      val->num = nval->num;
  } else {
    lval_del(val);
    val = lval_err(ERR_UNKNOWN_FUNCTION, "function: '%s'", op);
  }
  return val;
}

LVal* eval_unary_op(LEnv* lenv, LVal* val, char* op) {
#ifdef DEBUG
  printf("handling unary '%s'\n", op);
#endif
  if (STR_EQ(op, "++") || STR_EQ(op, "inc"))
    ++(val->num);
  else if (STR_EQ(op, "--") || STR_EQ(op, "dec"))
    --(val->num);
  else if (STR_EQ(op, "-") || STR_EQ(op, "neg"))
    val->num = -val->num;
  else {
    LTHROWIF(val, 1, ERR_UNKNOWN_FUNCTION, op);
  }
  return val;
}
