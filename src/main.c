#include <stdio.h>
#include <stdlib.h>

#include "eval.h"
#include "lval.h"
#include "parser.h"

#if defined WIN32 || defined __MINGW32__ || defined __MINGW64__
#include "win/readline.h"
#else
#include <readline/history.h>
#include <readline/readline.h>
#endif

#define VERSION "0.4"
#define PROMPT "lissy> "

void repl() {
  printf("Lissy v%s\n", VERSION);
  LEnv* env = lenv_new();

  mpc_result_t ast;
  for (;;) {
    char* input = readline(PROMPT);
    if (!input)
      break;
    if (strlen(input) == 0)
      continue;

    add_history(input);
    int correct = parse_str(input, &ast);
    if (correct) {
      LVal* expr = lval_read(ast.output);
#ifdef DEBUG
      lval_println(expr);
#endif
      LVal* result = eval(env, expr);
      mpc_ast_delete(ast.output);
      lval_println(result);
      lval_del(result);
    }

    free(input);
  }

  lenv_del(env);
}

// TODO: lissy can be called in interpreter mode (i.e. argv[1] is source file)
int main(int argc, char* argv[]) {
  init_parser();
  repl();
  del_parser();
  return 0;
}
