#include "lenv.h"
#include "builtin.h"
#include "util.h"

LEnv* lenv_new() {
  LEnv* env = INIT_LENV;
  env->count = 0;
  env->syms = NULL;
  env->vals = NULL;
  lenv_add_builtins(env);
  return env;
}

void lenv_del(LEnv* env) {
  for (int i = 0; i < env->count; i++) {
    free(env->syms[i]);
    lval_del(env->vals[i]);
  }
  free(env->syms);
  free(env->vals);
  free(env);
}

LVal* lenv_get(LEnv* env, LVal* key) {
  for (int i = 0; i < env->count; i++)
    if (STR_EQ(env->syms[i], key->sym))
      return lval_copy(env->vals[i]);

  char buf[1024];
  sprintf(buf, "unknown variable '%s'!", key->sym);
  LTHROWIF(key, 1, ERR_BAD_SYM, buf);
}

void lenv_put(LEnv* env, LVal* key, LVal* val) {
  for (int i = 0; i < env->count; i++)
    if (STR_EQ(env->syms[i], key->sym)) {
      lval_del(env->vals[i]);
      env->vals[i] = lval_copy(val);
      return;
    }

  int idx = env->count++;
  env->syms = realloc(env->syms, sizeof(char*) * env->count);
  env->vals = realloc(env->vals, sizeof(LVal*) * env->count);
  env->syms[idx] = strdup(key->sym);
  env->vals[idx] = lval_copy(val);
}

void lenv_add_builtin(LEnv* lenv, char* name, lbuiltin func) {
  LVal* key = lval_sym(name);
  LVal* val = lval_fun(func);
  lenv_put(lenv, key, val);
  lval_del(key);
  lval_del(val);
}

void lenv_add_builtins(LEnv* lenv) {
  ADD_BUILTIN_ALIAS(lenv, "++", "inc", inc);
  ADD_BUILTIN_ALIAS(lenv, "--", "dec", dec);
  ADD_BUILTIN(lenv, "neg", neg);

  ADD_BUILTIN_ALIAS(lenv, "+", "add", add);
  ADD_BUILTIN_ALIAS(lenv, "-", "sub", sub);
  ADD_BUILTIN_ALIAS(lenv, "*", "mul", mul);
  ADD_BUILTIN_ALIAS(lenv, "/", "div", div);
  ADD_BUILTIN_ALIAS(lenv, "%", "mod", mod);
  ADD_BUILTIN_ALIAS(lenv, "^", "pow", pow);

  ADD_BUILTIN(lenv, "min", min);
  ADD_BUILTIN(lenv, "max", max);

  ADD_BUILTIN(lenv, "head", head);
  ADD_BUILTIN(lenv, "tail", tail);
  ADD_BUILTIN(lenv, "list", list);
  ADD_BUILTIN(lenv, "join", join);
  ADD_BUILTIN(lenv, "eval", eval);
  ADD_BUILTIN(lenv, "length", length);

  ADD_BUILTIN(lenv, "let", let);
}
