#ifndef LENV_H
#define LENV_H

#include "lval.h"
#include "util.h"

#define INIT_LENV malloc(sizeof(LEnv))
#define ADD_BUILTIN(e, sym, name) lenv_add_builtin((e), (sym), builtin_##name)
#define ADD_BUILTIN_ALIAS(e, sym, alias, name)                                 \
  ADD_BUILTIN((e), (sym), name);                                               \
  ADD_BUILTIN((e), (alias), name)

struct lenv {
  int count;
  char** syms;
  LVal** vals;
};

LEnv* lenv_new();
void lenv_del(LEnv* env);

LVal* lenv_get(LEnv* env, LVal* key);
void lenv_put(LEnv* env, LVal* key, LVal* val);

void lenv_add_builtin(LEnv* lenv, char* name, lbuiltin func);
void lenv_add_builtins(LEnv* lenv);

#endif
