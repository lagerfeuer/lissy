#include "lval.h"
#include "util.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char* lval_type(LVAL_TYPE type) {
  switch (type) {
  case LVAL_NUM:
    return "Number";
  case LVAL_SYM:
    return "Symbol";
  case LVAL_FUN:
    return "Function";
  case LVAL_QEXPR:
    return "Q-Expression";
  case LVAL_SEXPR:
    return "S-Expression";
  case LVAL_ERR:
    return "Error";
  default:
    return "Unknown";
  }
}

//==============================================================================
// Constructors
//==============================================================================
LVal* lval_err(ERROR_TYPE err_type, char* fmt, ...) {
  LVal* lval = INIT_LVAL;
  lval->type = LVAL_ERR;
  lval->err.type = err_type;
  lval->err.msg = NULL;

  if (fmt == NULL)
    return lval;

  va_list va;
  va_start(va, fmt);
  unsigned int size = 1024;
  lval->err.msg = malloc(size);
  vsnprintf(lval->err.msg, size - 1, fmt, va);
  va_end(va);

  return lval;
}

LVal* lval_num(long num) {
  LVal* lval = INIT_LVAL;
  lval->type = LVAL_NUM;
  lval->num = num;
  return lval;
}

LVal* lval_sym(char* s) {
  LVal* lval = INIT_LVAL;
  lval->type = LVAL_SYM;
  lval->sym = strdup(s);
  return lval;
}

LVal* lval_fun(lbuiltin fun) {
  LVal* lval = INIT_LVAL;
  lval->type = LVAL_FUN;
  lval->fun = fun;
  return lval;
}

LVal* lval_sexpr() {
  LVal* lval = INIT_LVAL;
  lval->type = LVAL_SEXPR;
  lval->count = 0;
  lval->cell = NULL;
  return lval;
}

LVal* lval_qexpr() {
  LVal* lval = INIT_LVAL;
  lval->type = LVAL_QEXPR;
  lval->count = 0;
  lval->cell = NULL;
  return lval;
}

//==============================================================================
// Destructor
//==============================================================================
void lval_del(LVal* lval) {
  switch (lval->type) {
  case LVAL_NUM:
    break;
  case LVAL_ERR:
    free(lval->err.msg);
    break;
  case LVAL_SYM:
    free(lval->sym);
    break;
  case LVAL_FUN:
    break;
  case LVAL_SEXPR:
  case LVAL_QEXPR:
    for (int i = 0; i < lval->count; i++)
      lval_del(lval->cell[i]);
    free(lval->cell);
    break;
  }
  free(lval);
}

//==============================================================================
// Expressions
//==============================================================================

LVal* lval_read_num(mpc_ast_t* node) {
  errno = 0;
  long num = strtol(node->contents, NULL, 10);
  return (errno == 0)
             ? lval_num(num)
             : lval_err(ERR_BAD_ARG, "could not convert number (strtol)");
}

LVal* lval_read(mpc_ast_t* node) {
#ifdef DEBUG
  printf("[READ]: %s\n", node->tag);
#endif
  if (STR_EQ(node->tag, ">")) {
    for (int i = 0; i < node->children_num; i++) {
      if (IS_SEXPR(node->children[i]) || IS_QEXPR(node->children[i]))
        return lval_read(node->children[i]);
    }
  }

  if (IS_NUM(node))
    return lval_read_num(node);
  if (IS_SYM(node))
    return lval_sym(node->contents);

  LVal* lval = NULL;
  if (IS_SEXPR(node))
    lval = lval_sexpr();
  if (IS_QEXPR(node))
    lval = lval_qexpr();

  for (int i = 0; i < node->children_num; i++) {
    char* contents = node->children[i]->contents;
    char* tag = node->children[i]->tag;
    if (STR_EQ(contents, "(") || STR_EQ(contents, ")") ||
        STR_EQ(contents, "{") || STR_EQ(contents, "}") || STR_EQ(tag, "regex"))
      continue;
    lval_add(lval, lval_read(node->children[i]));
  }

  return lval;
}

LVal* lval_add(LVal* lval, LVal* val) {
  lval->cell = realloc(lval->cell, sizeof(LVal*) * ++(lval->count));
  lval->cell[lval->count - 1] = val;
  return lval;
}

LVal* lval_pop(LVal* lval, int i) {
  LVal* val = lval->cell[i];
  memmove(&lval->cell[i], &lval->cell[i + 1],
          sizeof(LVal*) * (--(lval->count) - i));
  lval->cell = realloc(lval->cell, sizeof(LVal*) * lval->count);
  return val;
}

LVal* lval_take(LVal* lval, int i) {
  LVal* val = lval_pop(lval, i);
  lval_del(val);
  return lval;
}

//==============================================================================
// Misc
//==============================================================================
LVal* lval_join(LVal* a, LVal* b) {
  while (b->count)
    a = lval_add(a, lval_pop(b, 0));
  lval_del(b);
  return a;
}

LVal* lval_copy(LVal* orig) {
  LVal* copy = INIT_LVAL;
  copy->type = orig->type;

  switch (orig->type) {
  case LVAL_ERR:
    copy->err.type = orig->err.type;
    if (orig->err.msg)
      copy->err.msg = strdup(orig->err.msg);
    break;
  case LVAL_NUM:
    copy->num = orig->num;
    break;
  case LVAL_SYM:
    copy->sym = strdup(orig->sym);
    break;
  case LVAL_FUN:
    copy->fun = orig->fun;
    break;
  case LVAL_SEXPR:
  case LVAL_QEXPR:
    copy->count = orig->count;
    for (int i = 0; i < copy->count; i++)
      copy->cell[i] = lval_copy(orig->cell[i]);
    break;
  }

  return copy;
}

//==============================================================================
// Printing
//==============================================================================
void lval_print(LVal* v) {
  switch (v->type) {
  case LVAL_NUM:
    printf("%li", v->num);
    break;
  case LVAL_SYM:
    printf("%s", v->sym);
    break;
  case LVAL_FUN:
    printf("<function>");
    break;
  case LVAL_SEXPR:
    lval_expr_print(v, '(', ')');
    break;
  case LVAL_QEXPR:
    lval_expr_print(v, '{', '}');
    break;

  case LVAL_ERR:
    switch (v->err.type) {
    case ERR_DIV_BY_ZERO:
      printf("Division by zero!");
      break;
    case ERR_BAD_ARG:
      printf("Got bad argument!");
      break;
    case ERR_BAD_SYM:
      printf("Got bad symbol!");
      break;
    case ERR_UNKNOWN_FUNCTION:
      printf("Unknown function!");
      break;
    case ERR_BAD_OP:
      printf("Not an operator!");
      break;
    case ERR_RANGE:
      printf("Number out of range!");
      break;
    }
    printf("\n  Info: %s", (v->err.msg) ? v->err.msg : "None");
    break;

  default:
    printf("ERROR: unknown LVal Type!\n");
    abort();
    break;
  }
}

void lval_expr_print(LVal* v, char open, char close) {
  putchar(open);
  for (int i = 0; i < v->count; i++) {
    lval_print(v->cell[i]);

    if (i != (v->count - 1))
      putchar(' ');
  }
  putchar(close);
}

void lval_println(LVal* lval) {
  lval_print(lval);
  puts("");
}
