#ifndef BUILTIN_H
#define BUILTIN_H

#include "eval.h"
#include "lenv.h"
#include "lval.h"

#define BUILTIN(name, sym)                                                     \
  LVal* builtin_##name(LEnv* e, LVal* v) { return builtin_op(e, v, (sym)); }

#define BUILTIN_QEXPR(name)                                                    \
  LVal* builtin_##name(LEnv* e, LVal* v) { return builtin_qexpr(e, v, #name); }

BUILTIN(inc, "inc")
BUILTIN(dec, "dec")
BUILTIN(neg, "neg")

BUILTIN(add, "+")
BUILTIN(sub, "-")
BUILTIN(mul, "*")
BUILTIN(div, "/")

BUILTIN(mod, "%")
BUILTIN(pow, "^")

BUILTIN(min, "min")
BUILTIN(max, "max")

BUILTIN_QEXPR(list)
BUILTIN_QEXPR(head)
BUILTIN_QEXPR(tail)
BUILTIN_QEXPR(eval)
BUILTIN_QEXPR(join)
BUILTIN_QEXPR(length)

LVal* builtin_let(LEnv* e, LVal* v) {
  for (int i = 0; i < v->count; i++) {
    LTHROWIF(v, v->cell[i]->type != LVAL_QEXPR, ERR_BAD_ARG,
             "arguments for 'let' need to be QExpr!");
  }

  for (int i = 0; i < v->count; i++) {
    LVal* tmp = v->cell[i];
    LVal* sym = tmp->cell[0];
    LVal* val = tmp->cell[1];
    LTHROWIF_FMT(
        v, sym->type != LVAL_SYM, ERR_BAD_ARG,
        "'let' passed wrong first argument in init list (need '%s', got '%s')!",
        "Symbol", lval_type(sym->type));
    LTHROWIF_FMT(v, val->type != LVAL_SYM && val->type != LVAL_NUM, ERR_BAD_ARG,
                 "'let' passed wrong second argument in init list (need '%s', "
                 "got '%s')!",
                 "Number", lval_type(val->type));
    lenv_put(e, sym, val);
  }

  lval_del(v);
  return lval_sexpr();
}
#endif
