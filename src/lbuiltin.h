struct lenv;
typedef struct lenv LEnv;
struct lval;
typedef struct lval LVal;
typedef LVal* (*lbuiltin)(LEnv*, LVal*);
