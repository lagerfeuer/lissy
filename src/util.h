#ifndef UTIL_H
#define UTIL_H

#define LTHROWIF_FMT(args, cond, err, msg, ...)                                \
  if ((cond)) {                                                                \
    lval_del(args);                                                            \
    return lval_err(err, msg, __VA_ARGS__);                                    \
  }

#define LTHROWIF(args, cond, err, msg)                                         \
  if ((cond)) {                                                                \
    lval_del(args);                                                            \
    return lval_err(err, msg);                                                 \
  }

#define STR_EQ(var, val) (strcmp((var), (val)) == 0)
#define STR_NEQ(var, val) (strcmp((var), (val)))

#define IS_NUM(node) strstr((node)->tag, "number")
#define IS_SYM(node) strstr((node)->tag, "symbol")
#define IS_SEXPR(node) strstr((node)->tag, "sexpr")
#define IS_QEXPR(node) strstr((node)->tag, "qexpr")

#define TYPE(node)                                                             \
  ((node->type == LVAL_SYM)                                                    \
       ? "symbol"                                                              \
       : ((node->type == LVAL_NUM)                                             \
              ? "number"                                                       \
              : ((node->type == LVAL_SEXPR)                                    \
                     ? "sexpr"                                                 \
                     : ((node->type == LVAL_QEXPR)                             \
                            ? "qexpr"                                          \
                            : ((node->type == LVAL_FUN) ? "function"           \
                                                        : "error")))))

#endif
