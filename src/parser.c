#include "parser.h"

int init_parser() {
  number = mpc_new("number");
  symbol = mpc_new("symbol");
  sexpr = mpc_new("sexpr");
  qexpr = mpc_new("qexpr");
  expr = mpc_new("expr");
  lissy = mpc_new("lissy");

  mpca_lang(MPCA_LANG_DEFAULT, "\
      number   : /-?[0-9]+/ ; \
      symbol   : /[a-zA-Z0-9_+\\-\\*\\/\\\\=<>!&%\\^]+/ ;\
      sexpr    : '(' <expr>* ')' ; \
      qexpr    : '{' <expr>* '}' ; \
      expr     : <number> | <symbol> | <sexpr> | <qexpr>; \
      lissy    : /^/ <sexpr>* /$/ | /^/ <qexpr>* /$/;",
            number, symbol, sexpr, qexpr, expr, lissy);
  return 0;
}

void handle_error(const int result, const mpc_result_t res) {
  if (!result) {
    mpc_err_print(res.error);
  }
#ifdef DEBUG
  else {
    puts("### AST ###");
    mpc_ast_print(res.output);
    puts("### END ###");
  }
#endif
}

int parse_str(char* input, mpc_result_t* res) {
  int result = mpc_parse("<stdin>", input, lissy, res);
  handle_error(result, *res);
  return result;
}

int parse_file(char* filename, mpc_result_t* res) {
  int result = mpc_parse_contents(filename, lissy, res);
  handle_error(result, *res);
  return result;
}

void del_parser() { mpc_cleanup(6, number, symbol, sexpr, qexpr, expr, lissy); }
