#ifndef READLINE_WIN_H
#define READLINE_WIN_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SIZE 2048
static char buffer[SIZE];

char* readline(char* prompt) {
  fputs(prompt, stdout);
  fgets(buffer, SIZE, stdin);
  char* line = (char*)malloc(strlen(buffer) + 1);
  if (!line)
    return NULL;
  strcpy(line, buffer);
  line[strlen(line) - 1] = '\0';
  return line;
}

void add_history(char* unused) {}

#endif
