# lissy
[![pipeline status](https://gitlab.com/lissy-lisp/lissy/badges/master/pipeline.svg)](https://gitlab.com/lissy-lisp/lissy/commits/master)

Minimal Lisp style interpreter based on [Build your own Lisp](http://buildyourownlisp.com).

## Builtins

### Operators

* `+`, `-`, `*`, `/`, `%`, `^`
* `add`, `sub`, `mul`, `div`, `mod`, `pow`

### Functions

* `min`, `max`

### Unary

* `++`, `--`, `-`
* `inc`, `dec`, `neg`

### Lists
* `head`, `tail`, `eval`, `list`, `length`, `join`

For example:
* `list`: `(list 1 2 3) -> {1 2 3}`
* `eval`: `(eval {+ 2 3}) -> 5`

### Variables
Only numbers supported so far!
* `(let {x 1} {y 2})`: sets `x = 1` and `y = 2`


## TODO
* [ ] fix memory leaks
* [x] add test suite
* [ ] support for float/double
* [ ] support for real numbers (e.g. 1 / 3 instead of .333333)
* [ ] support for complex numbers
* [x] support for lists
* [ ] support for autocompletion in repl
* [ ] variables and functions (e.g. environment)
